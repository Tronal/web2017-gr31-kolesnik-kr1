<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 06.05.2017
 * Time: 18:14
 */

namespace Maxim\HelpdeskBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller{
    public function indexAction($username = false){
        return $this->render("MaximHelpdeskBundle:User/Admin:index.html.twig");
    }
}