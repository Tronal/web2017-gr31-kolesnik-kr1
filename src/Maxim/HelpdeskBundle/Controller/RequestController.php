<?php

namespace Maxim\HelpdeskBundle\Controller;

use Maxim\HelpdeskBundle\Entity\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Request controller.
 *
 */
class RequestController extends Controller
{
    /**
     * Lists all request entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $role = $this->getUser()->getRoles()[0];
        if (!strcmp($role->getName(), "ROLE_ADMIN")) {
            $requests = $em->getRepository('MaximHelpdeskBundle:Request')->findAll();
            //$requests = $em->getRepository('MaximHelpdeskBundle:Request')->getAll2();
            //$requests = $em->getRepository('MaximHelpdeskBundle:Request')->getRequestOnIdUser($this->getUser()->getId());
        } else {

            //$requests = $em->getRepository('MaximHelpdeskBundle:Request')->findBy(["user" => $this->getUser()->getId()]);
            $requests = $em->getRepository('MaximHelpdeskBundle:Request')->getRequestOnIdUser($this->getUser()->getId());
        }

        //$requests = $em->getRepository('MaximHelpdeskBundle:Request')->findAll();

        return $this->render('request/index.html.twig', array(
            'requests' => $requests,
        ));
    }

    /**
     * Creates a new request entity.
     *
     */
    public function newAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $req = new Request();
        $form = $this->createForm('Maxim\HelpdeskBundle\Form\RequestForm', $req);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $req->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($req);
            $em->flush();

            return $this->redirectToRoute('request_show', array('id' => $req->getId()));
        }

        return $this->render('request/new.html.twig', array(
            'req' => $req,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a request entity.
     *
     */
    public function showAction(Request $req)
    {
        $deleteForm = $this->createDeleteForm($req);

        return $this->render('request/show.html.twig', array(
            'req' => $req,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing request entity.
     *
     */
    public function editAction(\Symfony\Component\HttpFoundation\Request $request, Request $req)
    {
        $deleteForm = $this->createDeleteForm($req);
        $editForm = $this->createForm('Maxim\HelpdeskBundle\Form\RequestForm', $req);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

//            return $this->redirectToRoute('request_edit', array('id' => $request->getId()));
        }

        return $this->render('request/edit.html.twig', array(
            'req' => $req,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a request entity.
     *
     */
    public function deleteAction(\Symfony\Component\HttpFoundation\Request $request, Request $req)
    {
        $form = $this->createDeleteForm($req);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($req);
            $em->flush();
        }

        return $this->redirectToRoute('request_index');
    }

    /**
     * Creates a form to delete a bid entity.
     *
     * @param Request $request The request entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Request $req)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('request_delete', array('id' => $req->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
