<?php

namespace Maxim\HelpdeskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();
        return $this->render('MaximHelpdeskBundle:Page:index.html.twig');
    }

}