<?php

namespace Maxim\HelpdeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * request
 *
 * @ORM\Table(name="request")
 * @ORM\Entity(repositoryClass="Maxim\HelpdeskBundle\Entity\Repository\RequestRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Request
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100)
     */
    private $category;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Maxim\HelpdeskBundle\Entity\User", inversedBy="requests")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return request
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return request
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return request
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set user
     *
     * @param \Maxim\HelpdeskBundle\Entity\User $user
     *
     * @return request
     */
    public function setUser(\Maxim\HelpdeskBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Maxim\HelpdeskBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

